#!/usr/bin/env bash

# Constants
black=`tput setaf 0`;
red=`tput setaf 1`;
green=`tput setaf 2`;
yellow=`tput setaf 3`;
blue=`tput setaf 4`;
magenta=`tput setaf 5`;
cyan=`tput setaf 6`;
white=`tput setaf 7`;
reset=`tput sgr0`;


# # Install nvm
# echo "${green}Instalando o nvm${reset}";
# export NVM_DIR="$HOME/.nvm" && (
#     GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone https://github.com/creationix/nvm.git "$NVM_DIR"
#     cd "$NVM_DIR"
#     #git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" origin`

#     # Get new tags from remote
#     git fetch --tags

#     # Get latest tag name
#     latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)

#     # Checkout latest tag
#     git checkout $latestTag

# ) && . "$NVM_DIR/nvm.sh"

# # Install Node-latest
# echo "${green}Instalando o node e latest npm${reset}";
# nvm install node --latest-npm

export DEBIAN_FRONTEND=noninteractive

echo "${green}Instalando o node e latest npm${reset}";
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install Yarn
echo "${green}Instalando o Yarn${reset}";
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt-get install -fy yarn

echo "echo 'export PATH=\"$PATH:$HOME/.yarn/bin\"' >> ~/.zshrc" | sudo sh

# echo "${green}Alterando o .zshrc${reset}";
# echo 'export NVM_DIR="$HOME/.nvm"' >> ~/.zshrc
# echo '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.zshrc
# echo '[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion' >> ~/.zshrc

# Cleanup
echo "${green}Limpando o apt${reset}";
sudo apt autoremove -y

echo "${cyan}Reboot?${reset}"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo init 6; break;;
        No ) exit;;
        # * ) echo "${red}Please answer 1 or 2.${reset}";;
    esac
done

cd ~
