#!/bin/bash

# Constants
black=`tput setaf 0`;
red=`tput setaf 1`;
green=`tput setaf 2`;
yellow=`tput setaf 3`;
blue=`tput setaf 4`;
magenta=`tput setaf 5`;
cyan=`tput setaf 6`;
white=`tput setaf 7`;
reset=`tput sgr0`;

clear

stop_and_remove_all_containers() {
	echo "${green}Stoping and removing all containers${reset}"
	for container in $( docker ps -a -q ); do
		docker stop $container
		docker rm $container
	done
}

remove_all_untagged_images() {
	# Maybe check
	# https://docs.docker.com/engine/reference/commandline/image_prune/
	echo "${green}Removing all untagged images${reset}"
	for image in $(docker images -aq -f dangling=true); do
		echo "Removing the image [ $image ]"
		docker rmi -f $image
	done
}

remove_all_images() {
	# SEARCH_VALUE=$1
	echo "${green}Removing all [ *$1* ] images${reset}"
	for image in $(docker images | awk -v search="$1" '$1 ~ search {print $3}'); do
		echo "${yellow}Removing the image [ $image ]${reset}"
		docker rmi -f $image
	done
}

update_all_images() {
	echo "${green}Updating all images${reset}"
	for image in $(docker images --format "{{.Repository}}:{{.Tag}}"); do
		echo "${green}Updating image [ ${blue}$image${reset} ]"
		docker pull $image
	done
}

stop_and_remove_all_containers

images_to_remove=( laravel metafy jetbrains gamerzclass phpstorm sail laradock )
images_to_remove+=( assinatura )
images_to_remove+=( fbb )
images_to_remove+=( carrefour )
images_to_remove+=( guia_de_execucao )
images_to_remove+=( hyst )
images_to_remove+=( credsystem )
images_to_remove+=( ensure_trade )
images_to_remove+=( unimed )
images_to_remove+=( tradeview )
images_to_remove+=( integracao_disparadores )
images_to_remove+=( lala )
images_to_remove+=( test )
images_to_remove+=( creative-saas )
images_to_remove+=( adonis )
images_to_remove+=( photobox )
images_to_remove+=( renderz )
images_to_remove+=( tech-api )

for i in "${images_to_remove[@]}"
do
	remove_all_images "$i"
done

remove_all_untagged_images
update_all_images
remove_all_untagged_images
