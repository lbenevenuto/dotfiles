#!/usr/bin/env bash

# Constants
black=`tput setaf 0`;
red=`tput setaf 1`;
green=`tput setaf 2`;
yellow=`tput setaf 3`;
blue=`tput setaf 4`;
magenta=`tput setaf 5`;
cyan=`tput setaf 6`;
white=`tput setaf 7`;
reset=`tput sgr0`;

if [[ -e $HOME/.dotfiles/.bootstrap ]]; then
	echo "Bootstrap has been executed already."
	echo "If you really want to run this again: rm \$HOME/.dotfiles/.bootstrap"
	exit 0
fi

export DEBIAN_FRONTEND=noninteractive

echo "${green}Instalando as dependencias${reset}"
sudo apt-get update && sudo apt-get install -fy \
    xclip \
    curl \
    git \
    tmux \
    exa \
    zsh \
    zsh-syntax-highlighting \
    powerline \
    fonts-powerline

git config --global core.excludesfile ~/.gitignore_global

# Install oh-my-zsh
echo "${green}Instalando o oh-my-zsh${reset}";
chsh -s $(which zsh);
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
# git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# ZSH_CUSTOM="$HOME/.oh-my-zsh"

echo 'HISTSIZE=500000' >> ~/.zshrc
echo 'ZSH_DISABLE_COMPFIX=true' >> ~/.zshrc
echo 'UPDATE_ZSH_DAYS=1' >> ~/.zshrc
echo 'HIST_STAMPS="dd.mm.yyyy"' >> ~/.zshrc
echo 'DISABLE_UPDATE_PROMPT=true' >> ~/.zshrc
echo 'source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> ~/.zshrc

# Colocando os plugins no lugar certo
sed --follow-symlinks -i -e "s/\(source \$ZSH\/oh-my-zsh.sh\)/plugins\+\=\(git-flow docker zsh-autosuggestions docker-compose gitignore helm perl kubectl cpanm common-aliases nvm npm yarn node composer laravel5 redis-cli supervisor ubuntu sudo debian command-not-found\)\n\1/" ~/.zshrc

# Install slkdfjslkdfj
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# Install powerlevel10k theme
echo "${green}Instalando o powerlevel10k theme${reset}";
git clone --depth=1 https://gitee.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
sed --follow-symlinks -i -r -e "s/^(ZSH_THEME=).*/\1\"powerlevel10k\/powerlevel10k\"/" ~/.zshrc
ln -s ~/.dotfiles/.p10k.zsh ~/.p10k.zsh

echo "" >> ~/.zshrc
echo '# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.' >> ~/.zshrc
echo "" >> ~/.zshrc
echo '[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh' >> ~/.zshrc

# linkando os aliases
echo "${green}Linkando os aliases${reset}";
ln -s ~/.dotfiles/.aliases ~/.aliases
echo 'source $HOME/.aliases' >> ~/.zshrc
echo 'ulimit -n 65536' >> ~/.zshrc

# Add XCLIP
echo "" >> ~/.zshrc
echo "# XClip" >> ~/.zshrc
cat ~/.dotfiles/XClip.sh >> ~/.zshrc
echo "" >> ~/.zshrc

# Cleanup
echo "${green}Limpando o apt${reset}";
sudo apt-get autoremove -y

echo "${cyan}Reboot?${reset}"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo init 6; break;;
        No ) exit;;
        # * ) echo "${red}Please answer 1 or 2.${reset}";;
    esac
done

cd ~