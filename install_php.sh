#!/usr/bin/env bash

# Constants
black=`tput setaf 0`;
red=`tput setaf 1`;
green=`tput setaf 2`;
yellow=`tput setaf 3`;
blue=`tput setaf 4`;
magenta=`tput setaf 5`;
cyan=`tput setaf 6`;
white=`tput setaf 7`;
reset=`tput sgr0`;

export DEBIAN_FRONTEND=noninteractive

echo "${green}Instalando o PHP CLI${reset}";
sudo apt update && sudo apt -fy install \
    build-essential \
    libpng-dev \
    libtool \
    autoconf \
    nasm \
    pngquant \
    libgd-dev \
    redis-tools \
    php-cli \
    php-curl \
    php-tokenizer \
    php-pdo-mysql \
    php-mysql \
    php-pdo-sqlite \
    php-sqlite3 \
    php-pdo-pgsql \
    php-pgsql \
    php-bz2 \
    php-bcmath \
    php-zip \
    php-mbstring \
    php-xml \
    php-gd \
    php-ctype \
    php-json \
    php-imagick \
    php-gmp \
    php-intl \
    php-uuid \
    php-pear \
    php-dev \
    php-redis

printf "\n" | sudo pecl install redis

# php -i | grep "Loaded Configuration File" | awk '{print $5}'
# sed -i -e "s/post_max_size.*/post_max_size = 256M/" php.ini
# sed -i -e "s/upload_max_filesize.*/upload_max_filesize = 256M/" php.ini

# echo "${green}Baixando e linkando o php docker composer${reset}";
# sudo docker pull composer
# ln -s ~/.dotfiles/composer.conf ~/.composer.conf
# echo 'source $HOME/.composer.conf' >> ~/.zshrc

echo "${green}Baixando e instalando o composer${reset}";
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/bin --filename=composer
php -r "unlink('composer-setup.php');"

echo "echo 'export PATH=\"$PATH:$HOME/.composer/vendor/bin\"' >> ~/.zshrc" | sudo sh

# Instalando o mozjpeg
echo "${green}Instalando o mozjpeg${reset}";
sudo rm -rf /tmp/mozjpeg
GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone https://github.com/mozilla/mozjpeg.git /tmp/mozjpeg
cd /tmp/mozjpeg
# git checkout $(git describe --tags $(git rev-list --tags --max-count=1))
git checkout $(git tag | sort -V | tail -1)
autoreconf -fiv
./configure
make all
#make test
sudo make install

# Cleanup
echo "${green}Limpando o apt${reset}";
sudo apt autoremove -fy

echo "${cyan}Reboot?${reset}"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo init 6; break;;
        No ) exit;;
        # * ) echo "${red}Please answer 1 or 2.${reset}";;
    esac
done

cd ~
