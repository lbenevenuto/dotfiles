#!/usr/bin/env bash

# Constants
black=`tput setaf 0`;
red=`tput setaf 1`;
green=`tput setaf 2`;
yellow=`tput setaf 3`;
blue=`tput setaf 4`;
magenta=`tput setaf 5`;
cyan=`tput setaf 6`;
white=`tput setaf 7`;
reset=`tput sgr0`;

export DEBIAN_FRONTEND=noninteractive

# instalando Docker
sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update && sudo apt-get install -fy \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

echo "${green}Instalando o docker${reset}";

sudo mkdir -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudp apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

sudo usermod -aG docker $USER

sudo docker run hello-world
sudo docker images

# Cleanup
echo "${green}Limpando o apt${reset}";
sudo apt autoremove -y

echo "${cyan}Reboot?${reset}"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo init 6; break;;
        No ) exit;;
        # * ) echo "${red}Please answer 1 or 2.${reset}";;
    esac
done

cd ~
