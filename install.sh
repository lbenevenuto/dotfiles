#!/usr/bin/env bash

# Constants
black=`tput setaf 0`;
red=`tput setaf 1`;
green=`tput setaf 2`;
yellow=`tput setaf 3`;
blue=`tput setaf 4`;
magenta=`tput setaf 5`;
cyan=`tput setaf 6`;
white=`tput setaf 7`;
reset=`tput sgr0`;

if [[ -e $HOME/.dotfiles/.bootstrap ]]; then
	echo "Bootstrap has been executed already."
	echo "If you really want to run this again: rm \$HOME/.dotfiles/.bootstrap"
	exit 0
fi

export DEBIAN_FRONTEND=noninteractive

echo "${green}Instalando as dependencias${reset}"
sudo apt update && sudo apt install -fy \
    apt-utils \
    iputils-ping \
    telnet \
    htop \
    curl \
    wget \
    git \
    git-flow \
    aptitude \
    openssl \
    ssh \
    vim \
    apt-transport-https \
    ca-certificates \
    gnupg2 \
    software-properties-common \
    build-essential \
    libcurl4-openssl-dev \
    pkg-config \
    autoconf \
    automake \
    libtool \
    nmap \
    acl \
    tmux \
    dkms \
    net-tools \
    libgmp-dev \
    libexpat-dev \
    nasm \
    locales \
    zip unzip bzip2

echo "${green}Full-Upgrade do sistema${reset}";
sudo aptitude -fy full-upgrade;

echo -e "Host github.com\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
echo -e "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config

# Setando o vim como editor principal
echo "${green}Alterando o editor default${reset}";
#sudo update-alternatives --config editor
echo 'export VISUAL="vim"' >> ~/.zshrc
echo 'export EDITOR="vim"' >> ~/.zshrc
echo "echo 'export VISUAL=\"vim\"' >> /etc/bash.bashrc" | sudo sh
echo "echo 'export EDITOR=\"vim\"' >> /etc/bash.bashrc" | sudo sh

# Configuracoes do VIM
echo "${green}Configuracoes do VIM${reset}";
echo "echo 'set nowrap' >> /etc/vim/vimrc" | sudo sh
echo "echo 'set hlsearch' >> /etc/vim/vimrc" | sudo sh

# Instalando o "no password" to user sudo
echo "echo '$USER ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers.d/$USER" | sudo sh

git config --global user.email "luiz@siffra.com.br"
git config --global user.name "Luiz Benevenuto"

# echo "echo '\n[inet_http_server]\nport = *:9001\nusername = user\npassword = 123' >> /etc/supervisor/supervisord.conf" | sudo sh

# Cleanup
echo "${green}Limpando o apt${reset}";
sudo apt autoremove -y

echo "${cyan}Reboot?${reset}"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo init 6; break;;
        No ) exit;;
        # * ) echo "${red}Please answer 1 or 2.${reset}";;
    esac
done

cd ~
