#!/usr/bin/env bash

# Constants
black=`tput setaf 0`;
red=`tput setaf 1`;
green=`tput setaf 2`;
yellow=`tput setaf 3`;
blue=`tput setaf 4`;
magenta=`tput setaf 5`;
cyan=`tput setaf 6`;
white=`tput setaf 7`;
reset=`tput sgr0`;

export DEBIAN_FRONTEND=noninteractive

echo "${green}Install APT dependencias${reset}";
sudo apt install -fy \
    build-essential \
    libssl-dev \
    libdb-dev \
    libxml2 \
    libxml2-dev \
    libgmp-dev \
    psmisc \
    libdbd-pg-perl \
    libpq-dev \
    openssl \
    lzma-dev \
    perltidy \
    cpanminus

#echo "${green}Install CPAN${reset}";
#export PERL_MM_USE_DEFAULT=1 && sudo cpan;

#echo "${green}Install PERL modules${reset}";
#curl -L https://cpanmin.us | perl - --sudo App::cpanminus

sudo cpanm YAML;
sudo cpanm Log::Log4perl;
sudo cpanm CPAN::Uploader;
# sudo cpanm Log::Log4perl Log::Dispatch::Email::MailSend JSON::Parse File::Slurp Benchmark Modern::Perl;
# sudo cpanm common::sense DDP MIME::Lite DBI;
# sudo cpanm ExtUtils::PkgConfig;
# sudo cpanm JSON Net::SFTP Net::FTP Spreadsheet::XLSX Spreadsheet::ParseXLSX List::Util;
# sudo cpanm Digest::MD5 DateTime Date::Parse Text::Soundex Net::Curl;
# sudo cpanm YADA;
# sudo cpanm WWW::UserAgent::Random File::Listing JSON::Any Locale::Currency::Format Date::Calc;
# sudo cpanm Date::Calc::XS Cpanel::JSON::XS JSON::XS;
# sudo cpanm File::Path String::Similarity File::Basename Scalar::Util POSIX;
# sudo cpanm Data::Peek DBD::Pg Log::Log4perl::Appender::SMTP Ref::Util;
# sudo cpanm utf8 File::Touch DBD::mysql String::Util DateTime::Format::Excel Excel::Writer::XLSX;
# sudo cpanm DBIx::Class;
# sudo cpanm Text::CSV_XS;
# sudo cpanm Date::Manip::Date;
# sudo cpanm Sys::Info;
# sudo cpanm Params::Util;
# sudo cpanm List::MoreUtils;
# sudo cpanm Crypt::OpenSSL::RSA IO::Socket::SSL LWP::Protocol::https;
# sudo cpanm ExtUtils::MakeMaker Data::Dumper::AutoEncode Log::Any Config::Any IO::Prompter;
# sudo cpanm Log::Any::Adapter::Dispatch ExtUtils::ModuleMaker Test::Perl::Critic;
# sudo cpanm Test::Pod Test::Pod::Coverage;
# sudo cpanm CPAN;

# update all perl packages
# echo "${green}Atualizando perl packages${reset}";
# sudo cpan -u;

# linkando as configuracoes do perltidyrc
echo "${green}Linkando perltidyrc${reset}";
ln -s ~/.dotfiles/.perltidyrc ~/.perltidyrc

# linkando os aliases
echo "${green}Linkando o .dataprinter${reset}";
cp ~/.dotfiles/.dataprinter ~/.dataprinter

# Cleanup
echo "${green}Limpando o apt${reset}";
sudo apt autoremove -y

echo "${cyan}Reboot?${reset}"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo init 6; break;;
        No ) exit;;
        # * ) echo "${red}Please answer 1 or 2.${reset}";;
    esac
done

cd ~
